#include <twist/rt/run.hpp>

#include <twist/ed/stdlike/atomic.hpp>
#include <twist/ed/stdlike/thread.hpp>
#include <twist/ed/wait/sys.hpp>

#include <fmt/core.h>

#include <chrono>

using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

class OneShotEvent {
  enum States : uint32_t {
    Init = 0,
    Fired = 1,
  };

 public:
  void Wait() {
    twist::ed::Wait(fired_, /*old=*/States::Init);
  }

  // One-shot
  void Fire() {
    // NB: before store
    auto wake_key = twist::ed::PrepareWake(fired_);

    fired_.store(States::Fired);
    twist::ed::WakeAll(wake_key);
  }

 private:
  twist::ed::stdlike::atomic<uint32_t> fired_{States::Init};
};

//////////////////////////////////////////////////////////////////////

int main() {
  twist::rt::Run([] {
    std::string data;
    OneShotEvent ready;

    twist::ed::stdlike::thread producer([&] {
      twist::ed::stdlike::this_thread::sleep_for(3s);

      data = "Hello";
      ready.Fire();
    });

    ready.Wait();
    fmt::println("Produced: {}", data);

    producer.join();
  });

  return 0;
}
