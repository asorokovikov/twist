# Build executable targets

add_custom_target(twist_build_examples ALL DEPENDS
        twist_example_deadlock
        twist_example_wait
        twist_example_spin
        twist_example_local
        twist_example_chrono
        twist_example_condvar
        twist_playground)

add_custom_target(twist_build_tests ALL DEPENDS
        twist_stress_tests)

add_custom_target(twist_build_all ALL DEPENDS
        twist_build_examples
        twist_build_tests)
