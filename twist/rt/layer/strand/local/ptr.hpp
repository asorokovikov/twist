#pragma once

#include <twist/rt/layer/strand/local/tls.hpp>

#include <wheels/core/noncopyable.hpp>

namespace twist::rt::strand {

//////////////////////////////////////////////////////////////////////

/*
 * Usage:
 *
 * ThreadLocalPtr<int> ptr;  // nullptr
 *
 * ptr = new int;  // store
 * int value = *ptr;  // load
 */

template <typename T>
class ThreadLocalPtr : private wheels::NonCopyable {
 public:
  using Ctor = std::function<T*()>;

  ThreadLocalPtr(Ctor ctor) {
    Init(ctor);
  }

  ThreadLocalPtr(T* init) {
    Init(/*ctor=*/[init] { return init; });
  }

  // Initialized with nullptr
  ThreadLocalPtr()
      : ThreadLocalPtr(nullptr) {
  }

  operator T*() {
    return Load();
  }

  ThreadLocalPtr& operator=(T* ptr) {
    Store(ptr);
    return *this;
  }

  T* Exchange(T* ptr) {
    T* old_ptr = Load();
    Store(ptr);
    return old_ptr;
  }

  // Usage: thread_local_ptr->Method();
  T* operator->() {
    return Load();
  }

  explicit operator bool() const {
    return Load() != nullptr;
  }

 private:
  void Init(Ctor ctor) {
    auto dtor = [](void* /*raw_ptr*/) {
      // Nop
    };
    slot_index_ = TLSManager::Instance().AcquireSlot({ctor, dtor});
  }

  T* Load() {
    return AccessSlot()->template LoadAs<T>();
  }

  void Store(T* ptr) {
    AccessSlot()->Store(ptr);
  }

  Slot* AccessSlot() {
    return TLSManager::Instance().Access(slot_index_);
  }

 private:
  size_t slot_index_;
};

}  // namespace twist::rt::strand

//////////////////////////////////////////////////////////////////////

// TWIST_DECLARE_TL_PTR

// Zero-overhead fiber-compatible twist-ed alternative
// to static thread_local pointer

#if defined(NDEBUG) && !defined(TWIST_FIBERS)

#define TWIST_DECLARE_TL_PTR_INIT(T, ptr, init) static thread_local T* ptr = init;

#define TWIST_DECLARE_TL_PTR(T, ptr) TWIST_DECLARE_TL_PTR_INIT(T, ptr, nullptr)

#else

#define TWIST_DECLARE_TL_PTR_INIT(T, ptr, init) static twist::rt::strand::ThreadLocalPtr<T> ptr{[] { return init; }};

#define TWIST_DECLARE_TL_PTR(T, ptr) static twist::rt::strand::ThreadLocalPtr<T> ptr{nullptr};

#endif
