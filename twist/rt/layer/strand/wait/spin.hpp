#pragma once

#if defined(TWIST_FIBERS)

#include <twist/rt/layer/fiber/wait/spin.hpp>

namespace twist::rt::strand {

using rt::fiber::SpinWait;
using rt::fiber::CpuRelax;

}  // namespace twist::rt::strand

#else

#include <twist/rt/layer/thread/wait/spin/wait.hpp>

namespace twist::rt::strand {

using rt::thread::SpinWait;
using rt::thread::CpuRelax;

}  // namespace twist::rt::strand

#endif
