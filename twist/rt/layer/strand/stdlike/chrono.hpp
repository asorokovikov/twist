#pragma once

#if defined(TWIST_FIBERS)

#include <twist/rt/layer/fiber/stdlike/chrono.hpp>

namespace twist::rt::strand {

using steady_clock = fiber::SteadyClock;
using system_clock = fiber::SteadyClock;

}  // namespace twist::rt::strand

#else

#include <chrono>

namespace twist::rt::strand {

using std::chrono::steady_clock;
using std::chrono::system_clock;

}  // namespace twist::rt::strand

#endif
