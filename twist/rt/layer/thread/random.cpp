#include <twist/rt/layer/thread/random.hpp>

#include <random>

namespace twist::rt::thread {

// Use thread-local pseudo-random uniform generators
// initialized with random seeds

//////////////////////////////////////////////////////////////////////

struct RandomUInt64Generator {
  using Value = uint64_t;
 public:
  RandomUInt64Generator()
      : twister_(ChooseSeed()),
        distr_(0, std::numeric_limits<Value>::max()) {
  }

  Value Produce() {
    return distr_(twister_);
  }

 private:
  static std::random_device::result_type ChooseSeed() {
    return std::random_device{}();
  }

 private:
  std::mt19937_64 twister_;
  std::uniform_int_distribution<Value> distr_;
};

//////////////////////////////////////////////////////////////////////

uint64_t GenerateRandomUInt64() {
  static thread_local RandomUInt64Generator generator;

  return generator.Produce();
}

}  // namespace twist::rt::thread
