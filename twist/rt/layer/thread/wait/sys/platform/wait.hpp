#pragma once

#include <cstdint>
#include <cstddef>
#include <climits>

namespace twist::rt {
namespace thread {

// Wrappers for platform-specific system calls (futex, ulock)

int PlatformWait(uint32_t* addr, uint32_t old);
int PlatformWaitTimed(uint32_t* addr, uint32_t expected, uint32_t millis);

int PlatformWake(uint32_t* addr, size_t count);

}  // namespace thread
}  // namespace twist::rt
