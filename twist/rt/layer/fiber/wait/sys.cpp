#include <twist/rt/layer/fiber/wait/sys.hpp>

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

namespace twist::rt::fiber {

namespace {

WaitKey WaitKeyFor(Atomic<uint32_t>& atomic) {
  return (void*)&atomic;
}

WaitQueue& WaitQueueBy(WaitKey key) {
  return Scheduler::Current()->Futex(key);
}

}  // namespace

void Wait(Atomic<uint32_t>& atomic, uint32_t old) {
  WaitQueue& wait_queue = WaitQueueBy(WaitKeyFor(atomic));
  while (atomic.load() == old) {
    wait_queue.Park();
  }
}

bool WaitTimed(Atomic<uint32_t>& atomic, uint32_t old, std::chrono::milliseconds timeout) {
  Scheduler* scheduler = Scheduler::Current();

  WaitQueue& wait_queue = scheduler->Futex(WaitKeyFor(atomic));

  VirtualTime& time = scheduler->Time();
  auto deadline = time.After(timeout);

  while (atomic.load() == old) {
    if (time.Now() >= deadline) {
      return false;
    }
    wait_queue.ParkTimed(deadline);
  }

  return true;
}

WakeKey PrepareWake(Atomic<uint32_t>& atomic) {
  return {WaitKeyFor(atomic)};
}

void WakeOne(WakeKey key) {
  WaitQueueBy(key.wait_key).WakeOne();
}

void WakeAll(WakeKey key) {
  WaitQueueBy(key.wait_key).WakeAll();
}

}  // namespace twist::rt::fiber
