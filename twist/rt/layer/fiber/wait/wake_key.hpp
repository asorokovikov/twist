#pragma once

#include <twist/rt/layer/fiber/runtime/wait_key.hpp>

namespace twist::rt::fiber {

struct WakeKey {
  WaitKey wait_key;
};

}  // namespace twist::rt::fiber
