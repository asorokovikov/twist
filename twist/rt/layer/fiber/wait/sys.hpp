#pragma once

#include <twist/rt/layer/fiber/stdlike/atomic.hpp>

#include <twist/rt/layer/fiber/wait/wake_key.hpp>

#include <chrono>

namespace twist::rt::fiber {

// Wait

void Wait(Atomic<uint32_t>& atomic, uint32_t old);
bool WaitTimed(Atomic<uint32_t>& atomic, uint32_t old, std::chrono::milliseconds timeout);

// Wake

WakeKey PrepareWake(Atomic<uint32_t>& atomic);

void WakeOne(WakeKey key);
void WakeAll(WakeKey key);

}  // namespace twist::rt::fiber
