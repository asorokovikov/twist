#pragma once

namespace twist::rt::fiber {

struct IFiberWatcher {
  virtual ~IFiberWatcher() = default;

  virtual void Completed() noexcept = 0;
};

}  // namespace twist::rt::fiber
