#pragma once

#include <sure/stack.hpp>

namespace twist::rt::fiber {

sure::Stack AllocateStack();

void ReleaseStack(sure::Stack);

void SetMinStackSize(size_t bytes);

}  // namespace twist::rt::fiber
