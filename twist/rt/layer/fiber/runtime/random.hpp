#pragma once

#include <cstdlib>
#include <random>

namespace twist::rt::fiber {

class RandomGenerator {
  using Impl = std::mt19937_64;

 public:
  RandomGenerator(size_t seed)
      : impl_(seed) {
  }

  void Seed(size_t seed) {
    impl_.seed(seed);
  }

  uint64_t Next() {
    return impl_();
  }

 private:
  Impl impl_;
};

}  // namespace twist::rt::fiber
