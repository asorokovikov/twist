#include <twist/rt/layer/fiber/runtime/wait_queue.hpp>

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

namespace twist::rt::fiber {

WaitQueue::WaitQueue(const std::string& descr) : descr_(descr) {
}

void WaitQueue::Park() {
  Scheduler* scheduler = Scheduler::Current();
  Fiber* caller = scheduler->RunningFiber();

  waiters_.PushBack(caller);
  scheduler->Suspend(/*where=*/descr_);
}

void WaitQueue::ParkTimed(VirtualTime::time_point /*deadline*/) {
  WHEELS_PANIC("ParkTimed is not implemented");
}

void WaitQueue::WakeOne() {
  if (waiters_.IsEmpty()) {
    return;
  }
#if defined(TWIST_FAULTY)
  Fiber* f = waiters_.PopRandom();
#else
  Fiber* f = waiters_.PopFront();
#endif
  f->Resume();
}

void WaitQueue::WakeAll() {
  while (!waiters_.IsEmpty()) {
    waiters_.PopFront()->Resume();
  }
}

}  // namespace twist::rt::fiber
