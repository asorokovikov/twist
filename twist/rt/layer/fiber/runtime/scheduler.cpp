#include <twist/rt/layer/fiber/runtime/scheduler.hpp>
#include <twist/rt/layer/fiber/runtime/stacks.hpp>
#include <twist/rt/layer/fiber/report/stacktrace.hpp>

#include <wheels/core/compiler.hpp>
#include <wheels/core/panic.hpp>

#include <fmt/core.h>
#include <fmt/color.h>

#if defined(TWIST_FAULTY)
#include <twist/rt/layer/fault/random/helpers.hpp>
#endif

#include <utility>
#include <iostream>
#include <sstream>

namespace twist::rt::fiber {

//////////////////////////////////////////////////////////////////////

static Scheduler* current_scheduler;

Scheduler* Scheduler::Current() {
  WHEELS_VERIFY(current_scheduler, "Not in fiber context");
  return current_scheduler;
}

struct SchedulerScope {
  SchedulerScope(Scheduler* scheduler) {
    WHEELS_VERIFY(!current_scheduler,
                  "Cannot run scheduler from another scheduler");
    current_scheduler = scheduler;
  }

  ~SchedulerScope() {
    current_scheduler = nullptr;
  }
};

//////////////////////////////////////////////////////////////////////

Scheduler::Scheduler(size_t seed) : random_(seed) {
}

// Operations invoked by running fibers

void Scheduler::SwitchTo(Fiber* fiber) {
  loop_context_.SwitchTo(fiber->Context());
}

void Scheduler::SwitchToScheduler() {
  running_->Context().SwitchTo(loop_context_);
}

// ~ System calls

Fiber* Scheduler::Spawn(FiberRoutine routine) {
  auto* fiber = CreateFiber(std::move(routine));
  Schedule(fiber);
  return fiber;
}

void Scheduler::Yield() {
  if (preempt_disabled_) {
    return;
  }
  running_->SetState(FiberState::Runnable);
  SwitchToScheduler();
}

void Scheduler::SleepFor(std::chrono::milliseconds delay) {
  Fiber* caller = RunningFiber();

  bool first = sleep_queue_.IsEmpty();
  sleep_queue_.Put(caller, time_.After(delay));
  if (first) {
    Spawn([this]() {
      TimeKeeper();
    });
  }
  caller->SetState(FiberState::Suspended);
  caller->SetWhere("sleep queue");
  SwitchToScheduler();
}

void Scheduler::Suspend(std::string_view where) {
  Fiber* caller = RunningFiber();
  caller->SetState(FiberState::Suspended);
  caller->SetWhere(where);

  SwitchToScheduler();

  if (WHEELS_UNLIKELY(caller->State() == FiberState::Deadlocked)) {
    ReportFromDeadlockedFiber(caller);
    SwitchToScheduler();
  }
}

void Scheduler::Resume(Fiber* waiter) {
  WHEELS_ASSERT(waiter->State() == FiberState::Suspended,
                "Unexpected fiber state");
  waiter->SetState(FiberState::Runnable);
  waiter->SetWhere("-");
  Schedule(waiter);
}

void Scheduler::Terminate() {
  running_->SetState(FiberState::Terminated);
  SwitchToScheduler();
}

Fiber* Scheduler::RunningFiber() const {
  return running_;
}

// Scheduling

void Scheduler::Seed(size_t seed) {
  random_.Seed(seed);
}

void Scheduler::Run(FiberRoutine main) {
  SchedulerScope scope(this);

  BeforeStart();

  Spawn(std::move(main))->SetWatcher(this);

  RunLoop();

  AfterStop();
}

// Main fiber completed
void Scheduler::Completed() noexcept {
  WHEELS_VERIFY(alive_.empty(), "There are alive fibers after main completion");
}

void Scheduler::BeforeStart() {
  ids_.Reset();
  time_.Reset();

  futex_.clear();

  preempt_disabled_ = false;

  switch_count_ = 0;
  futex_count_ = 0;
}

void Scheduler::Tick() {
  time_.AdvanceBy(std::chrono::microseconds(10));
}

void Scheduler::RunLoop() {
  while (!run_queue_.IsEmpty()) {
    Tick();
    Fiber* next = PickReadyFiber();
    Step(next);
    Dispatch(next);
  }
}

void Scheduler::PollSleepQueue() {
  auto now = time_.Now();
  while (Fiber* f = sleep_queue_.Poll(now)) {
    Resume(f);
  }
}

bool Scheduler::IsIdle() const {
  return run_queue_.IsEmpty() && !sleep_queue_.IsEmpty();
}

void Scheduler::TimeKeeper() {
  while (!sleep_queue_.IsEmpty()) {
    PollSleepQueue();

    if (IsIdle()) {
      time_.AdvanceTo(sleep_queue_.NextDeadLine());
      continue;
    } else if (!sleep_queue_.IsEmpty()) {
      Yield();  // Reschedule poller
    } else {
      break;
    }
  }
}

Fiber* Scheduler::PickReadyFiber() {
#if defined(TWIST_FAULTY)
  return run_queue_.PopRandom();
#else
  return run_queue_.PopFront();
#endif
}

void Scheduler::Step(Fiber* fiber) {
  ++switch_count_;
  fiber->SetState(FiberState::Running);
  ++fiber->steps_;
  running_ = fiber;
  SwitchTo(fiber);
  running_ = nullptr;
}

void Scheduler::Dispatch(Fiber* fiber) {
  switch (fiber->State()) {
    case FiberState::Runnable:  // From Yield
      Schedule(fiber);
      break;
    case FiberState::Suspended:  // From Suspend
      // Do nothing
      break;
    case FiberState::Terminated:  // From Terminate
      Destroy(fiber);
      break;
    default:
      WHEELS_PANIC("Unexpected fiber state");
      break;
  }
}

void Scheduler::Schedule(Fiber* fiber) {
  run_queue_.PushBack(fiber);
}

Fiber* Scheduler::CreateFiber(FiberRoutine routine) {
  auto stack = AllocateStack();
  FiberId id = ids_.Generate();

  Fiber* fiber = new Fiber(this, std::move(routine), std::move(stack), id);

  alive_.insert(fiber);
  return fiber;
}

void Scheduler::Destroy(Fiber* fiber) {
  alive_.erase(fiber);

  if (auto* watcher = fiber->Watcher(); watcher != nullptr) {
    watcher->Completed();
  }

  ReleaseStack(std::move(fiber->Stack()));

  delete fiber;
}

// O(1)
size_t Scheduler::AliveCount() const {
  return alive_.size();
}

uint64_t Scheduler::GenerateRandomUInt64() {
  return random_.Next();
}

void Scheduler::AfterStop() {
  CheckDeadlock();
}

void Scheduler::CheckDeadlock() {
  if (run_queue_.IsEmpty() && AliveCount() > 0) {
    // Validate
    for (auto* f : alive_) {
      WHEELS_VERIFY(f->State() == FiberState::Suspended, "Internal error");
    }
    ReportDeadlockAndDie();
  }
}

void Scheduler::ReportDeadlockAndDie() {
  fmt::print(fg(fmt::color::red),
             "Deadlock detected in fiber scheduler: {} fibers blocked\n",
             alive_.size());
  fmt::print("Steps made: {}\n", switch_count_);

  fmt::println("");  // Blank line

  for (auto* fiber : alive_) {
    fiber->SetState(FiberState::Deadlocked);
    running_ = fiber;
    SwitchTo(fiber);
  }

  fmt::print("REPORT END\n");
  fflush(stdout);

  WHEELS_PANIC("Deadlock detected");
}

void Scheduler::ReportFromDeadlockedFiber(Fiber* me) {
  fmt::print(fg(fmt::color::fuchsia), "Fiber #{}", me->Id());
  fmt::print(" blocked at ");
  fmt::print(fg(fmt::color::red), "{}\n", me->Where());

  // Stack trace
  std::ostringstream trace_out;
  PrintStackTrace(trace_out);
  const auto trace = trace_out.str();
  if (!trace.empty()) {
    fmt::println("{}", trace);
  }
}

WaitQueue& Scheduler::Futex(WaitKey key) {
  ++futex_count_;
  auto [it, _] = futex_.try_emplace(key, "futex");
  return it->second;
}

size_t Scheduler::FutexCount() const {
  return futex_count_;
}

}  // namespace twist::rt::fiber
