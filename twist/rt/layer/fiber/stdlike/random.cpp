#include <twist/rt/layer/fiber/stdlike/random.hpp>

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

namespace twist::rt::fiber {

RandomDevice::result_type RandomDevice::Generate() {
  return Scheduler::Current()->GenerateRandomUInt64();
}

}  // namespace twist::rt::fiber
