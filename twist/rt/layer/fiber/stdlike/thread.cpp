#include <twist/rt/layer/fiber/stdlike/thread.hpp>

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

#include <wheels/core/assert.hpp>

namespace twist::rt::fiber {

//////////////////////////////////////////////////////////////////////

ThreadLike::ThreadLike(FiberRoutine routine) {
  fiber_ = Scheduler::Current()->Spawn(std::move(routine));
  fiber_->SetWatcher(this);
  state_ = State::Attached;
}

ThreadLike::~ThreadLike() {
  WHEELS_VERIFY(state_ == State::Detached, "Thread join or detach required");
}

ThreadLike::ThreadLike(ThreadLike&& that) {
  MoveFrom(that);
}

ThreadLike& ThreadLike::operator =(ThreadLike&& that) {
  Reset();
  MoveFrom(that);
  return *this;
}

void ThreadLike::Completed() noexcept {
  fiber_ = nullptr;
  state_ = State::Completed;
  join_.WakeOne();
}

bool ThreadLike::joinable() const {
  return state_ != State::Detached;
}

void ThreadLike::detach() {
  WHEELS_VERIFY(state_ != State::Detached, "Thread has already been detached");

  // Attached or Completed

  if (state_ == State::Attached) {
    // Detach
    fiber_->SetWatcher(nullptr);
  } else {
    // Do nothing
  }

  // Move to Detached state
  fiber_ = nullptr;
  state_ = State::Detached;
}

void ThreadLike::join() {
  WHEELS_VERIFY(state_ != State::Detached, "Thread has been detached");

  // Attached or Completed

  if (state_ == State::Attached) {
    // Wait for completion
    join_.Park();
  } else {
    // Do nothing
  }

  // Move to Detached state
  fiber_ = nullptr;
  state_ = State::Detached;
}

void ThreadLike::Reset() {
  if (state_ == State::Attached) {
    fiber_->SetWatcher(nullptr);
  }

  fiber_ = nullptr;
  state_ = State::Detached;
}

void ThreadLike::MoveFrom(ThreadLike& that) {
  fiber_ = std::exchange(that.fiber_, nullptr);
  state_ = std::exchange(that.state_, State::Detached);

  if (state_ == State::Attached) {
    fiber_->SetWatcher(this);
  }
}

//////////////////////////////////////////////////////////////////////

}  // namespace twist::rt::fiber
