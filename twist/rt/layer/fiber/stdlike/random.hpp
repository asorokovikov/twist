#pragma once

#include <twist/rt/layer/fiber/runtime/random.hpp>

#include <limits>

namespace twist::rt::fiber {

class RandomDevice {
 public:
  using result_type = unsigned int;

  result_type operator()() {
    return Generate();
  }

  // Characteristics

  double entropy() const noexcept {
    return 32;
  }

  static constexpr result_type min() {
    return 0;
  }

  static constexpr result_type max() {
    return std::numeric_limits<result_type>::max();
  }

 private:
  result_type Generate();
};

}  // namespace twist::rt::fiber
