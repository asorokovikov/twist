#pragma once

#include <twist/rt/layer/strand/stdlike/mutex.hpp>
#include <twist/rt/layer/strand/stdlike/thread.hpp>

namespace twist::rt {
namespace fault {

class FaultyMutex {
 public:
  FaultyMutex();

  // std::mutex-like / Lockable
  void lock();      // NOLINT
  bool try_lock();  // NOLINT
  void unlock();    // NOLINT

 private:
  twist::rt::strand::stdlike::mutex impl_;
  twist::rt::strand::stdlike::thread_id owner_id_;
};

}  // namespace fault
}  // namespace twist::rt
