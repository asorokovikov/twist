#include <twist/rt/layer/fault/random/uint64.hpp>

#if defined(TWIST_FIBERS)

#include <twist/rt/layer/fiber/runtime/scheduler.hpp>

namespace twist::rt::fault {

// Deterministic randomness for fibers

uint64_t RandomUInt64() {
  return fiber::Scheduler::Current()->GenerateRandomUInt64();
}

}  // namespace twist::rt::fault

#else

#include <twist/rt/layer/thread/random.hpp>

namespace twist::rt::fault {

uint64_t RandomUInt64() {
  return thread::GenerateRandomUInt64();
}

}  // namespace twist::rt::fault

#endif
