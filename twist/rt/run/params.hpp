#pragma once

#include <cstdlib>

namespace twist::rt {

struct Params {
  size_t seed = 42;

  Params& Seed(size_t value) {
    seed = value;
    return *this;
  }
};

}  // namespace twist::rt
