#pragma once

#include <twist/rt/run/env/this.hpp>

namespace twist::test {

inline bool KeepRunning() {
  return rt::this_env->KeepRunning();
}

}  // namespace twist::test
